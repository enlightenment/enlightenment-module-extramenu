/*  Copyright (C) 2008-2015 Davide Andreoli (see AUTHORS)
 *
 *  This file is part of extramenu.
 *  extramenu is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  extramenu is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with extramenu.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef E_MOD_MAIN_H
#define E_MOD_MAIN_H

/*************************************************************/
/** HERE YOU CAN CONFIGURE THE MODULE FOR YOUR NEEDS        **/
/*************************************************************/
#define MENU_USER_DIR ".e/e/extra_menu"
#define MENU_SYSTEM_DIR "/usr/share/menus"
//#define MENU_SYSTEM_DIR "/etc/xdg/menus"
#define DEFAULT_MENU_PLACEMENT "main/7"
/*************************************************************/

typedef struct _Extra_Menu  Extra_Menu;

struct _Extra_Menu
{
   const char *menu;
   Efreet_Menu *efreet_menu;
   E_Int_Menu_Augmentation *maug;
};

EAPI extern E_Module_Api e_modapi;

EAPI void *e_modapi_init     (E_Module *m);
EAPI int   e_modapi_shutdown (E_Module *m);
EAPI int   e_modapi_save     (E_Module *m);


#endif
